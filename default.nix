with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "explore-dev-with-k8-env";
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [
    docker-machine-kvm2
    libvirt
    nodejs-10_x
    qemu

    kubectl
    kubernetes
    kubernetes-helm
    minikube
  ];

  shellHook = ''
    alias gst="git status"
    alias vim="nvim"
  '';
}
