# Explore development with K8s

## Motivation

Explore if [draft](https://github.com/Azure/draft) is a viable option for setting up a local development environment.

## Testing out Draft

1. Start Minikube: `minikube start --vm-driver=kvm2 --memory 4096`
2. Get Tiller running: `helm init`
3. Reuse Minikube's Docker daemon: `eval $(minikube docker-env)`
4. Load dashboard: `minikube dashboard`
5. Get the `draft` binary and put it on your `$PATH`, then run `draft init`
6. In your application folder: `draft create && draft up`
7. You can check that a pod is fired up with your application. `draft connect` forwards it to your host machine so you can test it out

```shell
$ cd example-app

$ draft create
Draft Up Started: 'example-app': 01D2F2N0EDA1QHQ6CQ63D64DQ9
example-app: Building Docker Image: SUCCESS ⚓  (1.0006s)
example-app: Releasing Application: SUCCESS ⚓  (2.1207s)
Inspect the logs with `draft logs 01D2F2N0EDA1QHQ6CQ63D64DQ9`

$ draft up

$ draft connect
Connect to example-app:8080 on localhost:32849
[example-app]:
[example-app]: > example-app@1.0.0 start /
[example-app]: > node index.js
[example-app]:
[example-app]: server is listening on 8080
[example-app]: /

$ curl localhost:32849
Hello Node.js Server!
```

## Packs

- https://github.com/Azure/draft/blob/master/docs/reference/dep-003.md
